package com.example.killingmesoftly;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;
import java.util.Calendar;

public class Form extends AppCompatActivity implements View.OnClickListener {
    private final String INFO = "¿Que sonará en tu funeral?" +
            "\nResponde a este test para saber que temazo sonará en tu funeral";
    private TextView textView;
    private String selectedDate;
    private EditText nombre, editDate;
    private RadioButton rock, reggaeton;
    private CheckBox fumar, beber, drogas, mafia, depor, apuestas;
    private Button btn;
    private DatePickerDialog.OnDateSetListener dateSetListener;

    int numResult = 0;
    boolean musica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form);
        textView = findViewById(R.id.info);
        textView.setText(INFO);
        editDate = findViewById(R.id.editDate);
        editDate.setOnClickListener(this);
        nombre = findViewById(R.id.editTextName);
        rock =findViewById(R.id.rock);
        reggaeton = findViewById(R.id.reggaeton);

        fumar = findViewById(R.id.fumar);
        beber = findViewById(R.id.beber);
        drogas = findViewById(R.id.drogas);
        mafia = findViewById(R.id.mafia);
        depor = findViewById(R.id.depor);
        apuestas = findViewById(R.id.apuesta);

        btn = findViewById(R.id.btn);
    }
    public void onClick(View v){
        switch (v.getId()){
            case R.id.editDate:
                showDatePickerDialog();
                break;
            case R.id.button:
                if(nombre.getText().toString().length() == 0 && nombre.getText().toString().contains(" ")){
                    Toast.makeText(this,"Debe introducir un Nombre", Toast.LENGTH_SHORT).show();
                }
                if(!rock.isChecked() && !reggaeton.isChecked()){
                    Toast.makeText(this,"Tiene que elegir una opcion de música", Toast.LENGTH_SHORT).show();
                }
                Intent intent = new Intent(this, Result.class);
                intent.putExtra("nombre", (Serializable) nombre);
                intent.putExtra("musica", musica);
                intent.putExtra("result", numResult);
                startActivity(intent);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + v.getId());
        }
    }

    public void showDatePickerDialog(){
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            selectedDate = day + " / " + (month+1) + " / " + year;
            editDate.setText(selectedDate);
        }
    });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
    public void radioButton(View v){
        if (rock.isChecked() == true){
            musica = true;
        }else if(reggaeton.isChecked() == true){
            musica = false;
        }
    }
    public void checked(View v){
        if(fumar.isChecked()){
            numResult++;
        }
        if(beber.isChecked()){
            numResult++;
        }
        if(drogas.isChecked()){
            numResult++;
        }
        if(mafia.isChecked()){
            numResult++;
        }
        if(depor.isChecked()){
            numResult++;
        }
        if(apuestas.isChecked()){
            numResult++;
        }
    }

}
